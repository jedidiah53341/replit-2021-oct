/* Perulangan adalah diulangnya baris kode tertentu selama suatu kondisi masih benar/true. 



For loop membutuhkan 3 parameter, yaitu kondisi awal variabel yang akan dievaluasi, kondisi evaluasi, dan terakhir adalah perubahan terhadap variabel tersebut di akhir setiap perulangan.
*/
for (let i = 0; i < 5; i++) {
    // Kode untuk dijalankan selam i kurang dari 5
  }

  /* For in membutuhkan 1 parameter berupa deklarasi 1 variabel yang akan diupdate seiring menelusuri suatu array. Variabel yang dideklarasikan untuk For in hanya menelusuri index dari array, dan tidak membaca nilainya.
  */

  var deretangka = [2,4,6,8,10];
  for (let x in deretangka) {
  console.log(deretangka[x]);
} 



/*For of juga butuh 1 parameter berupa deklarasi 1 variabel yang akan diupdate seiring menelusuri suatu array. Namun, perbedaannya dengan for in adalah bahwa variabel yang dideklarasikan tersebut memiliki nilai dari setiap indeks yang ditelusuri. Hal ini membuat kita tak perlu menuliskan 'deretangka[x]' , namun cukup 'x' */

for (x of deretangka) {
  console.log(x);
} 

/*While loop memerlukan 1 parameter berisi kondisi. Program akan mengevaluasi terlebih dahulu apakah kondisi tersebut terpenuhi atau tidak -- sebelum menjalankan kode-nya. Untuk while loop, kita perlu mendefinisikan perubahan terhadap variabel yang dievaluasi kondisinya untuk mencegah loop tak terhingga. */

 i = 0;
while (i < 5) {
// Kode untuk dijalankan selama i kurang dari 5
i++;
} 

/* Do..while adalah variasi while loop dimana letak kata kunci 'do' beserta parameternya diletakkan setelah baris kode yang ingin dilakukan. Oleh karena itu, baris kode tersebut akan dijalankan setidaknya sekali, sekalipun kondisi yang diminta di parameter 'do' tidak terpenuhi. */

 i = 6;
do {
// Baris kode ini akan dijalankan sekali sekalipun i lebih besar dari 5
i++;
}
while (i < 5); 

// Nomor 2
function printNumber(num) {
  for (let y = 0; y <= num; y++) {
    console.log(y);
  }
}

//Test printNumber(3)
printNumber(3);

//Nomor 3
function cekGanjilGenap(num) {
  if( num % 2 == 0 ) {
return "Genap";
 } else {
return "Ganjil";
 }
 }
//Test cekGanjilGenap(4)
console.log(cekGanjilGenap(4));

// Nomor 4
function deretGanjilGenap(num) {
  for ( z=1;z<=num;z++) {
    
    if (z % 2 == 0) {
      console.log(z + " adalah genap");
    } else {
      console.log(z + " adalah ganjil");
    }}}

console.log(deretGanjilGenap(5));

//Nomor 5
const kegiatan = [
['bangun',7],
['rapihkan kamar',7.15],
['sarapan',7.30],
['mandi',7.45],
['mengumpulkan preclass Fazztrack',8]
];

for (x in kegiatan) {
  console.log("Pada pukul " + kegiatan[x][1] + " saya " + kegiatan[x][0]);
}


for( let i = 8; i >= 4; i --) {

    console.log(i);
  
}