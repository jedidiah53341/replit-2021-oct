/* 
1. Algoritma adalah urutan kegiatan untuk menyelesaikan pekerjaan tertentu. Dalam algoritma, setiap langkah perlu diselesaikan terlebih dahulu sebelum bisa lanjut ke langkah selanjutnya. */

console.log("\n");
console.log("Algoritma Memasak Mie Instan :");
console.log("1. Siapkan alat dan bahan, yaitu 1 bungkus mie instan, air, kompor, panci, piring, dan garpu.");
console.log("2. Masukkan air ke panci.");
console.log("3. Letakkan panci ke kompor, nyalakan kompor.");
console.log("4. Ketika mulai mendidih, masukkan mie ke panci.");
console.log("5. Selagi menunggu mie matang, tuang bumbu-bumbu mie ke piring.");
console.log("6. Saat mie matang, matikan kompor.");
console.log("7. Pindahkan mie di kompor ke piring yang sudah dituangi bumbu.");
console.log("8. Mie siap dikonsumsi.");

console.log("\n");
console.log("Algoritma Mendaftar Bootcamp Fazztrack :");
console.log("1. Kunjungi situs fazztrack.com .");
console.log("2. Klik tombol Daftar di sisi kanan-atas layar.");
console.log("3. Di layar pemilihan program, pilih Fullstack Mobile.");
console.log("4. Lengkapi data yang diminta, seperti jenis kelamin, asal sekolah, nomor telepon. Kemudian klik lanjut.");
console.log("5. Buka email yang anda gunakan untuk mendaftar di Fazztrack. Buka email dari Fazztrack yang menginformasikan link join grup Discord.");
console.log("6. Join grup Discord tersebut.");

/* Data structures adalah istilah komputer yang merujuk pada metode untuk menyimpan data. Berikut adalah penjelasan singkat mengenai beberapa struktur data :

1. Array, yaitu himpunan data yang disimpan secara berurutan. Urutan data dalam array umumnya dimulai dari index 0 untuk data pertama.
2. Linked List, yaitu himpunan data yang tersimpan berurutan dan terhubung dengan data disebelahnya/selanjutnya. Oleh karena itu, mengakses data linked list perlu dimulai dari data pertama-nya.
3. Stack adalah himpunan data yang dimana data yang masuk selalu ditempatkan di urutan awal stack, sedangkan data yang dapat diambil adalah selalu yang ditempatkan di urutan pertama stack( Last In First Out ).
4. Queue adalah himpunan data yang dimana data yang masuk selalu ditempatkan di urutan awal stack, sedangkan data yang dapat diambil adalah selalu yang ditempatkan di urutan terakhir queue( First In First Out ).
5. Binary tree, yaitu data yang disimpan dalam urutan hierarkis, dimana setiap data hanya dapat memiliki maksimal 2 data di satu hirarki dibawahnya. */