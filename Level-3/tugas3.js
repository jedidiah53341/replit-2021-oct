/* Cara membuat function di Javascript diawali dengan keyword function, kemudian dilanjutkan dengan nama fungsi,kemudian tanda kurung ' ( ) ' untuk memasukkan argumen, dan kurung kurawal ' { } ' untuk mendefinisikan rangkaian kegiatan dalam fungsi tersebut. */

function salam(nama) {
  console.log("Halo " + nama + ", selamat pagi!");
}

function tambah(bilanganPertama,bilanganKedua) {
  return bilanganPertama+bilanganKedua;
}

penjelasanArrowFunction = () => {
console.log("Arrow function adalah cara mendefinisikan fungsi dengan sedikit lebih ringkas. Arrow function adalah fitur baru Ecmascript 6. Dengan arrow function, kita tak perlu menuliskan keyword 'function' ketika mendeklarasikan fungsi. ");
}

perkalian = (bilanganPertama,bilanganKedua) => {
return bilanganPertama*bilanganKedua;
}


// Test fungsi
salam("Jedidiah");
console.log(tambah(2,2));
penjelasanArrowFunction();
console.log(perkalian(2,2));