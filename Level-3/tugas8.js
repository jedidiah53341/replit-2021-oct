// Nomor 1
fazztrack = (num) => {
for(y=1;y<=num;y++) {
  if ( y%10 == 0 ) {
    console.log("Jedidiah")
  } else if (y%3 ==0 && y%5 ==0) {
    console.log("fazztrack");
  } else if (y%5==0) {
    console.log("track");
  } else if (y%3 ==0) {
    console.log("fazz");
  }  else {
    console.log(y);
  }
}
}
fazztrack(15);

// Nomor 2
genap = (num) => {
  if ( num % 2 == 0) {
    return "true";
  } else {
    return false;
  }
}

ganjil = (num) => {
  if ( num % 2 != 0) {
    return "true";
  }else {
    return false;
  }
}

console.log(ganjil(3));
console.log(ganjil(6));
console.log(genap(4));

//Soal 3 

cekParameter = (param) => {
  if (typeof(param)=="string") {
    return "Parameter " + param + " adalah String!";
  } else if (typeof(param)=="number") {
    return "Parameter " + param + " adalah Number!";
  } else if (typeof(param)=="boolean") {
    return "Parameter " + param + " adalah Boolean!";
  } else if (Array.isArray(param) ) {
    return "Parameter " + param + " adalah Array!";
  } else  {
    return "Parameter " + param + " adalah Object!";
  }
  }

console.log(cekParameter([1,2,9]));
console.log(cekParameter({nama:'Rudi',umur:20}));