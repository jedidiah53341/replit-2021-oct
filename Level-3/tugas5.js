/* Array adalah himpunan data yang disimpan secara berurutan. Elemen dalam array dapat berupa string, number, array, dan objek. Elemen dalam array dapat diakses dari dengan format 'namaArray[i]', dimana i adalah index item yang ingin diakses.

Objek adalah data terhadap suatu objek, dimana data tersebut disimpan dalam format key-value pairs. Dalam penggunaan key-value pairs, untuk dapat mengakses data/value, maka yang kita referensikan adalah key-nya, dan bukan urutan munculnya pasangan key-value tersebut dalam objek.
*/

var hobi = ["membaca","menulis","belajar pemrograman"];

var saya = {
  nama:"Jedidiah",
  umur:22,
  hobi:hobi
};

var sekolah = [
{namaSekolah:"SMP Ora et Labora BSD",jurusan:"IPA",tahunMasuk:2011,tahunKeluar:2014},
{namaSekolah:"SMA Ora et Labora BSD",jurusan:"IPA",tahunMasuk:2014,tahunKeluar:2017},
{namaSekolah:"Institut Pertanian Bogor",jurusan:"T.Industri Pertanian",tahunMasuk:2017,tahunKeluar:2021}
];

//test array dan objek
console.log(hobi[2]);
console.log(saya['umur']);
console.log(sekolah[2].namaSekolah);

const jawaban = "Menurut saya, kutip 1(\') dan kutip 2(\") tidak berbeda dan keduanya diperbolehkan. Namun begitu, ketika kita ingin menyusun sebuah string yang mengandung kutip 2, maka sebaiknya kita gunakan kutip satu untuk mengawali dan mengakhiri keseluruhan string tersebut, sebab apabila kita gunakan kutip 2 diawal string, maka string tersebut akan berhenti di titik dimana kita tuliskan tanda kutip 2. Cara lain untuk mengatasi ini adalah dengan menggunakan special character sequence .";

console.log(jawaban);

