/* Cara membuat conditional if else adalah dengan diawali katakunci if, kemudian menuliskan kondisi yang ingin diuji dalam tanda kurung ' () ', kemudian menuliskan aktivitas yang ingin dijalankan ketika kondisi tersebut benar, di kurung kurawal '{}' . 

Jika hanya membandingkan uji 2 kondisi, maka setelah membandingkan dengan keyword 'if', kita dapat tuliskan 'else' untuk menuliskan aktivitas yang ingin dijalankan ketika kondisi tersebut false.

Jika membandingkan lebih dari 2 kondisi, maka setelah if yang pertama kita perlu gunakan 'else if', kemudian gunakan 'else' untuk kondisi yang terakhir.
*/

function cekParameter(x) {
  if (x != null) {
    return "Ini parameternya " + x;
  } else {
    return "Tidak ada parameter yang diberikan!";
  }
}

salam = (jam) => {
  if ( jam <= 10 ) {
    return "Selamat Pagi!";
  } else if ( jam <= 14) {
    return "Selamat Siang!";
  } else if ( jam <= 18) {
    return "Selamat Sore!";
  } else if ( jam <= 24) {
    return "Selamat Malam!";
  } else {
    return "Jam salah/tidak ada";
  }
}


// Test fungsi
console.log(cekParameter());
console.log(cekParameter(5));
console.log(salam(5));
console.log(salam(19));

/* Perbedaannya adalah == hanya mengecek apakah nilai kedua variabel sama, sedangkan === mengecek apakah nilai-nya sama dan jenis datanya sama. Selain itu, != hanya mengecek apakah nilai kedua variabel tak sama, sedangkan !== mengecek apakah nilai-nya tak sama ataupun jenis data-nya tidak sama. Untuk tujuan perbandingan, menurut saya lebih baik menggunakan == dan != saja, karena dengan demikian kita tak perlu makan waktu untuk mengecek kesamaan jenis data-nya.
*/
