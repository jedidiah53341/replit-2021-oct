//Nomor 1
/* Database adalah wadah untuk menyimpan data, umumnya dalam bentuk baris dan kolom. Perbedaannya adalah SQL merupakan relational database, sedangkan NoSQL non-relational database. Relational database memiliki makna bahwa kita perlu menyusun terlebih dahulu jenis-jenis data apa saja yang ingin disimpan/ kolom apa saja yang ada di setiap tabel-nya. Non-relational database memiliki makna bahwa kita tak perlu menyusun tabel untuk menyimpan data-nya, melainkan setiap entri data dapat dihubungkan secara key-value, dokumen, Column family, dan graph. */

//Nomor 2
/*Saya hanya pernah belajar SQL. Saya sudah paham keyword untuk create database, table, mendefinisikan kolom dan jenis data-nya, SELECT, memasukkan-update-delete record, menggunakan WHERE, ORDER BY, GROUP BY, JOIN-ON, dan LIMIT. Namun saya masih belum bisa cepat membaca nested SELECT.*/

//Nomor 3
/*Contoh queri diambil dari subscription Progate saya
-- Select the name and price column of all products with a price higher than the price of "grey hoodie"
SELECT name, price
FROM items
WHERE price > (
SELECT price
from items
WHERE name = "grey hoodie" )
;
*/

//Nomor 4
/* Contoh find all dengan NoSQL MongoDB
MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("mydb");
  //Find all documents in the customers collection:
  dbo.collection("customers").find({}).toArray(function(err, result) {
    if (err) throw err;
    console.log(result);
    db.close();
  });
});
*/
